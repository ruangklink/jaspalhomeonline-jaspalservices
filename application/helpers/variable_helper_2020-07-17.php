<?php
	/**
	 * Init Variable Helper Class
	 * Use for support api service
	 * Copyright (c) 2016 @Adyim
	 * @category   VariableHelper
	 * @package    ProductManagementWebservice
	 * @copyright  Copyright (c) 2016 @Adyim
	 * @license    None
	 * @version    1.0.0, 2016-07-01
	 */

	//////////////////////////
	// ### Product Type ### //
	// simple, configurable //
	//////////////////////////

	////////////////////////////////////////////////////////////////////////////
	// 								### Category ### 						  //
	//	1. Default Category [Null] = 2										  //
	//	2. Collection [Adjust on Magento CMS] = 56							  //
	//	3. Arts & Decor [Bathroom, Decorative, Dinning, Textile Access] = 62  //
	//	4. Bed Linen [Bed Linen] = 63										  //
	//	5. Bath Linen [Bath Accessories] = 64								  //
	//	6. Mattress & Bed Ensemles [Mattress, Bed Ensemles] = 65    		  //
	//	7. Floor Coverung [Rugs] = 66										  //
	//	8. White Fillings [White Fillings] = 67								  //
	////////////////////////////////////////////////////////////////////////////

	class VariableHelper {
		/* Default Variable */
		protected $viewDF; // Store View ADMIN / DEFAULT Id
		protected $viewEN; // Store View EN Id
		protected $viewTH; // Store View TH Id
		protected $setDF; // default Product Set ID
		protected $webDF; // Jaspal Wesite ID
		protected $cateDefault = 2;
		protected $cateCollect = 56;
		protected $cateArts = 62;
		protected $cateBed = 63;
		protected $cateBath = 64;
		protected $cateMatt = 65;
		protected $cateFloor = 66;
		protected $cateWhite = 67;

	  public function __construct($params) {
	   	/* init variable */
	   	$this->viewDF = $params['viewDF'];
	    $this->viewEN = $params['viewEN'];
	    $this->viewTH = $params['viewTH'];
	    $this->setDF = $params['setDF'];
	    $this->webDF = $params['webDF'];
	  }

	  /**
		 * init product info data, use for match api
		 * @param  array $lists
		 * @return array
		 */
		public function initProdInfo($lists){
			// init product data json to array
			$list = array();
		  $products = (array) json_decode($lists['data'], true);

			if($lists['type']=='new' || $lists['type']=='update'){
				// init action
				$list['action'] = $lists['type'];
				// init product type
				$list['type'] = $products['PRODUCT_TYPE'];
				// init product category
				$list['category'] = $this->initCategoryValue($products['PDGRP_CODE']);
				// init product model
				$list['model'] = $products['PDMODEL_DESC'];
				// init product info
		    $list['prod_info'] = array(
					'product_type'=>@$products['PRODUCT_TYPE'],
					'sku'=>@$products['PROD_CODE'],
					'name_th'=>@$products['PDNAME_TH'],
					'name_en'=>@$products['PDNAME_EN'],
					'short_description_th'=>@$products['SHORT_DESC_TH'],
					'short_description_en'=>@$products['SHORT_DESC_EN'],
					'description_th'=>@$products['DESC_TH'],
					'description_en'=>@$products['DESC_EN'],
					'material_detail_th'=>@$products['MATERIAL1_TH'],
					'material_detail_en'=>@$products['MATERIAL1'],
					'dimension_description_th'=>@$products['DIMENSION_DESC_TH'],
					'dimension_description_en'=>@$products['DIMENSION_DESC_EN'],
					'uom_th'=>@$products['UOM_TH'],
					'uom_en'=>@$products['UOM_EN'],
					'weight'=>@$products['WEIGHT'],
					'status'=>(!empty($lists['PIC_FILE']) ? '1' : '2'),
					'price'=>@$products['UNIT_PRICE']
				);
		    	// init product attributes info
		    $list['prod_attr'] = array(
					'product_group_en'=>@$products['PDGRP_DESC'],
					'product_group_th'=>@$products['PDGRP_TH'],
					'product_type_en'=>@$products['PDTYPE_DESC'],
					'product_type_th'=>@$products['PDTYPE_TH'],
					'product_design_en'=>@$products['PDSIZE_TH'],
					'product_design_th'=>@$products['PDDSGN_TH'],
					'product_color_en'=>@$products['COLOR1'], //PDCOLOR_DESC
					'product_color_th'=>@$products['PDCOLOR_TH'],
					'product_size_en'=>@$products['PDSIZE_DESC'],
					'product_size_th'=>@$products['PDSIZE_DESC']
				);
		    // init product attributes code info
		    $list['prod_code'] = array(
		    	array('key'=>'code_barcode', 'value'=>@$products['BAR_CODE']),
		    	array('key'=>'code_brnd', 'value'=>@$products['PDBRND_CODE']),
		    	array('key'=>'code_brnddesc', 'value'=>@$products['PDBRND_DESC']),
		    	array('key'=>'code_color', 'value'=>@$products['PDCOLOR_CODE']),
		    	array('key'=>'code_color1', 'value'=>@$products['COLOR1']),
				  array('key'=>'code_color2', 'value'=>@$products['COLOR2']),
		    	array('key'=>'code_dim', 'value'=>@$products['DIMENSION']),
				  array('key'=>'code_maten', 'value'=>@$products['MATERIAL2']),
				  array('key'=>'code_matth', 'value'=>@$products['MATERIAL2_TH']),
		    	array('key'=>'code_misc', 'value'=>@$products['PDMISC_CODE']),
		    	array('key'=>'code_miscdesc', 'value'=>@$products['PDMISC_DESC']),
		    	array('key'=>'code_pddsgn', 'value'=>@$products['PDDSGN_CODE']),
		    	array('key'=>'code_pdgrp', 'value'=>@$products['PDGRP_CODE']),
		    	array('key'=>'code_pdmodel', 'value'=>@$products['PDMODEL_CODE']),
		    	array('key'=>'code_pdnameth', 'value'=>@$products['PROD_TNAME']),
					array('key'=>'code_pdnameen', 'value'=>@$products['PROD_ENAME']),
		    	array('key'=>'code_pdsize', 'value'=>@$products['PDSIZE_CODE']),
					array('key'=>'code_prod', 'value'=>@$products['PROD_CODE']),
					array('key'=>'code_prodclass', 'value'=>@$products['PROD_CLASS']),
					array('key'=>'code_prodline', 'value'=>@$products['PROD_LINE']),
					array('key'=>'code_prodst', 'value'=>@$products['PROD_ST']),
					array('key'=>'code_prodtype', 'value'=>@$products['PROD_TYPE']),
					array('key'=>'code_refno', 'value'=>@$products['REF_NO']),
					array('key'=>'code_type', 'value'=>@$products['PDTYPE_CODE']),
					array('key'=>'code_uom', 'value'=>@$products['UOM_CODE'])
				);
				// init product inventory info
				$list['prod_inv'] = array(
					'qty'=>@$products['STOCK_QTY'],
					'is_in_stock'=>'1'
				);
				// init product images info
				$list['prod_img'] = array(
					array('content'=>@$lists['PIC_FILE'], 'mine'=>'image/jpeg'),
					array('content'=>@$lists['PIC_FILE2'], 'mine'=>'image/jpeg'),
					array('content'=>@$lists['PIC_FILE3'], 'mine'=>'image/jpeg'),
					array('content'=>@$lists['PIC_FILE4'], 'mine'=>'image/jpeg'),
					array('content'=>@$lists['PIC_FILE5'], 'mine'=>'image/jpeg')
				);
		  } else if($lists['type']=='stock') {
		   	// init data type inventory
				$list['action'] = $lists['type'];
		   	$list['type'] = 'stock';
		   	$list['prod_info'] = array('sku'=>@$products['PROD_CODE'], 'status'=>'1');
				$list['prod_inv'] = array('qty'=>@$products['StockQty'], 'is_in_stock'=>'1');
		  } else {
				// init data type store
				$list['action'] = $lists['type'];
		   	$list['type'] = 'store';
				$list['prod_info'] = array('sku'=>@$products['PROD_CODE'], 'status'=>'1');
				$list['prod_attr'] = array('product_find_in_shop'=>@$products['StockQty']);
			}

			return $list;
		}

		/**
		 * Init CategoryID value
		 * @param  string $category
		 * @return integer
		 */
		public function initCategoryValue($category){
			if($category=='BR' || $category=='DC' || $category=='DN' || $category=='TA')
				$cate_val = $this->cateArts; // Arts & Decor
			else if($category=='BD')
				$cate_val = $this->cateBed; // Bed Linen
			else if($category=='JB')
				$cate_val = $this->cateBath; // Bath Linen
			else if($category=='MU' || $category=='S7' || $category=='BX' || $category=='ST' || $category=='HB')
				$cate_val = $this->cateMatt; // Mattress & Bed Ensemles
			else if($category=='RU')
				$cate_val = $this->cateFloor; // Floor Coverung
			else if($category=='SL')
				$cate_val = $this->cateWhite; // White Fillings
			else
				$cate_val = $this->cateDefault; // Default Category

			return $cate_val;
		}

		/**
		 * Init product inventory value
		 * @param  array $list
		 * @return array
		 */
		public function initProductInventoryValue($list, $qty = 0, $flag = false){
			if($list['type']=='simple'){
				$params = array(
					'qty'=> ($flag ? $qty : $list['prod_inv']['qty']),
					'is_in_stock'=>$list['prod_inv']['is_in_stock'],
					'manage_stock'=>1,
					'use_config_manage_stock'=>1,
					'min_qty'=>0,
					'use_config_min_qty'=>1,
					'min_sale_qty'=>1,
					'use_config_min_sale_qty'=>1,
					'max_sale_qty'=>10000,
					'use_config_max_sale_qty'=>1,
					'is_qty_decimal'=>0,
					'backorders'=>1,
					'use_config_backorders'=>1,
					'notify_stock_qty'=>1,
					'use_config_notify_stock_qty'=>1
				);
			} else {
				$params = array(
					'is_in_stock'=>$list['prod_inv']['is_in_stock'],
					'manage_stock'=>1,
					'use_config_manage_stock'=>1
				);
			}

			return $params;
		}

		/**
		 * Init product value
		 * @param  array $list
		 * @param  array $attr
		 * @param  array $inv
		 * @param  integer $visible
		 * @param  array  $associate
		 * @return array
		 */
		public function initProductStoreValue($attr){
			return array(
				'websites' => array($this->webDF),
				'additional_attributes' => array('multi_data'=>$attr)
			);		
		}

		/**
		 * Init product value
		 * @param  array $list
		 * @param  array $attr
		 * @param  array $inv
		 * @param  integer $visible
		 * @param  array  $associate
		 * @return array
		 */
		public function initProductValue($list, $attr, $inv, $visible, $associate=array()){
			// 1 = Not visible, 2 = Catalog, 3 = Search, 4 = Catalog/Search
			if($visible == 0) {
				if($list['type']=='simple' || $visible == 1){
					$params = array(
					    'categories' => array($list['category']),
					    'websites' => array($this->webDF),
					    'name' => $list['prod_info']['name_en'],
					    'short_description' => $list['prod_info']['short_description_en'],
					    'description' => $list['prod_info']['description_en'],
					    'weight' => $list['prod_info']['weight'],
					    'status' => $list['prod_info']['status'],
					    'price' => $list['prod_info']['price'],
					    'tax_class_id' => '0',
					    'additional_attributes' => array('single_data'=>$attr),
					    'stock_data' => $inv
					);
				} else {
					$params = array(
					    'categories' => array($list['category']),
					    'websites' => array($this->webDF),
					    'name' => $list['prod_info']['name_en'],
					    'short_description' => $list['prod_info']['short_description_en'],
					    'description' => $list['prod_info']['description_en'],
					    'weight' => $list['prod_info']['weight'],
					    'status' => $list['prod_info']['status'],
					    'price' => $list['prod_info']['price'],
					    'tax_class_id' => '0',
					    'associated_skus' => $associate,
					    'stock_data' => $inv
					);
				}
			} else {
				if($list['type']=='simple' || $visible == 1){
					$params = array(
					    'categories' => array($list['category']),
					    'websites' => array($this->webDF),
					    'name' => $list['prod_info']['name_en'],
					    'short_description' => $list['prod_info']['short_description_en'],
					    'description' => $list['prod_info']['description_en'],
					    'weight' => $list['prod_info']['weight'],
					    'status' => $list['prod_info']['status'],
					    'visibility' => $visible,
					    'price' => $list['prod_info']['price'],
					    'tax_class_id' => '0',
					    'additional_attributes' => array('single_data'=>$attr),
					    'stock_data' => $inv
					);
				} else {
					$params = array(
					    'categories' => array($list['category']),
					    'websites' => array($this->webDF),
					    'name' => $list['prod_info']['name_en'],
					    'short_description' => $list['prod_info']['short_description_en'],
					    'description' => $list['prod_info']['description_en'],
					    'weight' => $list['prod_info']['weight'],
					    'status' => $list['prod_info']['status'],
					    'visibility' => $visible,
					    'price' => $list['prod_info']['price'],
					    'tax_class_id' => '0',
					    'associated_skus' => $associate,
					    'stock_data' => $inv
					);
				}
			}

			return $params;
		}

		/**
		 * initProductValueTH description
		 * @param  array
		 * @return array
		 */
		public function initProductValueTH($list){
			return $params = array(
				'name' => $list['prod_info']['name_th'],
				'short_description' => $list['prod_info']['short_description_th'],
				'description' => $list['prod_info']['description_th'],
				'additional_attributes' => array('single_data'=>array(
					array('key'=>'product_dimension', 'value'=>$list['prod_info']['dimension_description_th']),
					array('key'=>'product_material', 'value'=>$list['prod_info']['material_detail_th']),
					array('key'=>'product_uom', 'value'=>$list['prod_info']['uom_th']),
					array('key'=>'product_model', 'value'=>$list['model'])
				))
			);
		}

		/**
		 * Init Product Attribute Value
		 * @param  array $list
		 * @param  array $option
		 * @param  integer $store
		 * @return array
		 */
		public function initProductAttrValue($list, $option, $store){
			$params = array(
				array(
					'key'=>'product_dimension',
					'value'=>($store==$this->viewDF ? $list['prod_info']['dimension_description_en'] : $list['prod_info']['dimension_description_th'])
				),
				array(
					'key'=>'product_material',
					'value'=>($store==$this->viewDF ? $list['prod_info']['material_detail_en'] : $list['prod_info']['material_detail_en'])
				),
				array(
					'key'=>'product_uom',
					'value'=>($store==$this->viewDF ? $list['prod_info']['uom_en'] : $list['prod_info']['uom_th'])
				),
				array('key'=>'product_color', 'value'=>$option['product_color']),
				array('key'=>'product_design', 'value'=>$option['product_design']),
				array('key'=>'product_group', 'value'=>$option['product_group']),
				array('key'=>'product_size', 'value'=>$option['product_size']),
				array('key'=>'product_type', 'value'=>$option['product_type']),
				array('key'=>'product_model', 'value'=>$list['model'])
			);

			foreach ($list['prod_code'] as $value) {
				array_push($params, $value);
			}

			return $params;
		}

		/**
		 * Init Product Attribute Store Value
		 * @param  array $list
		 * @param  array $option
		 * @param  integer $store
		 * @return array
		 */
		public function initProductAttrStoreValue($list, $option, $store){
			return array(array('key'=>'product_find_in_shop', 'value'=>$option['product_find_in_shop']));
		}

		/**
		 * Init Product Attribute Master Value
		 * @param  array $list
		 * @param  array $option
		 * @param  string $store
		 * @return array
		 */
		public function initProductAttrMasterValue($list, $option, $store){
			$params = array(
				array(
			    	'key'=>'product_dimension',
			    	'value'=>($store==$this->viewDF ? $list['prod_info']['dimension_description_en'] : $list['prod_info']['dimension_description_th'])
			    ),
			    array(
			    	'key'=>'product_material',
			    	'value'=>($store==$this->viewDF ? $list['prod_info']['material_detail_en'] : $list['prod_info']['material_detail_en'])
			    ),
			    array(
			    	'key'=>'product_uom',
			    	'value'=>($store==$this->viewDF ? $list['prod_info']['uom_en'] : $list['prod_info']['uom_th'])
			    ),
			    array('key'=>'product_design', 'value'=>$option['product_design']),
			    array('key'=>'product_group', 'value'=>$option['product_group']),
			    array('key'=>'product_type', 'value'=>$option['product_type']),
			    array('key'=>'product_model', 'value'=>$list['model']),
			    array('key'=>'amconf_simple_price', 'value'=>1)
			);

			return $params;
		}
	}