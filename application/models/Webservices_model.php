<?php
	class Webservices_model extends CI_Model {
		function __construct() {
			parent::__construct();
			date_default_timezone_set('Asia/Bangkok');
		}

		function insertConnectLog($status, $params = [])   {
			$fields = [
				"status" => $status,
				"error_code" => $params['MessageCode'],
				"error_desc" => $params['MessageDesc'],
				"total_add" => ($params['TotalRecordAdd'] > 0 ? $params['TotalRecordAdd'] : 0),
				"total_update" => ($params['TotalRecordUpdate'] > 0 ? $params['TotalRecordUpdate'] : 0),
				"total_stock" => ($params['TotalRecordStock'] > 0 ? $params['TotalRecordStock'] : 0),
				"total_store" => ($params['TotalRecordStore'] > 0 ? $params['TotalRecordStore'] : 0)
			];

			$this->db->set('createdate', 'NOW()', FALSE);
			$this->db->insert("connection", $fields);
			return $this->db->insert_id();
		}

		function insertConnectLogTest($status, $params = [])   {
			$fields = [
				"status" => $status,
				"error_code" => $params['MessageCode'],
				"error_desc" => $params['MessageDesc'],
				"total_add" => ($params['TotalRecordAdd'] > 0 ? $params['TotalRecordAdd'] : 0),
				"total_update" => ($params['TotalRecordUpdate'] > 0 ? $params['TotalRecordUpdate'] : 0),
				"total_stock" => ($params['TotalRecordStock'] > 0 ? $params['TotalRecordStock'] : 0),
				"total_store" => ($params['TotalRecordStore'] > 0 ? $params['TotalRecordStore'] : 0)
			];

			$this->db->set('createdate', 'NOW()', FALSE);
			$this->db->insert("connection_test", $fields);
			return $this->db->insert_id();
		}

		function getConnectLog($id){
			$this->db->where("id", $id);
			return $this->db->get('connection')->result_array();
		}
		
		function getAllConnectLog(){
			return $this->db->get('connection')->result_array();
		}

		function clearTemporary(){
			$this->db->truncate('temp');
		}

		function getAllTemporary($type){
			$this->db->where("type", $type);
			return $this->db->get('temp')->result_array();
		}

		function getTemporary($type, $status){
			if($status==5){
				$this->db->where("type", $type);
			} else {
				$this->db->where(["status"=>$status,"type"=>$type]);
				$this->db->limit(1);
			}

			return $this->db->get('temp')->result_array();
		}

		function addTemporary($type, $total, $status){
			for ($i=1; $i<=$total ; $i++) {
				$fields = [
					"type" => $type,
					"number" => $i,
					"status" => $status,
				];
				$this->db->insert("temp", $fields);
			} 
		}

	  function updateTemporary($id, $status, $trans_id=''){
			$fields = [
				"status" => $status,
        "trans_id" => $trans_id,
			];
			$this->db->update("temp", $fields, ["id"=>$id]);
		}
		
		function deleteTemporary($id){			
			$this->db->where('id', $id);
			$this->db->delete('temp');	
		}

		function getAllTransLog(){
			$result = $this->db->get('transactions')->result_array();
			return $result;
		}

		function getTransLog($params){
			$this->db->select('a.*, b.PIC_FILE, b.PIC_FILE2, b.PIC_FILE3, b.PIC_FILE4, b.PIC_FILE5');
			$this->db->from('transactions as a');
			$this->db->where(['a.trans_id'=>$params['trans_id'],'a.status'=>0]);
			$this->db->join('transactions_images as b', 'b.TRANS_ID = a.trans_id', 'left outer');
			$result = $this->db->get()->result_array();
			return $result;
		}

		function insertTransLog($list, $type) {
			$id = 0;
			if($type=='new' || $type=='update') {
				$img = array('PIC_FILE','PIC_FILE2','PIC_FILE3','PIC_FILE4','PIC_FILE5');
				$newlist = array();
				$images = array();
				foreach ($list as $key => $val) {
					if (!in_array($key, $img)) {
						/* remove tabs, spaces, newlines, etc. */
						$str1 = str_replace(array("\r\n","\n","\\n","\\r\\n"), '<br/>', $val);
						$str2 = str_replace(array("\t","\r"), " ", $str1);
						$newlist[$key] = $str2;
					} else {
						/* init image data */
						$images[$key] = trim(str_replace(array("\r\n","\r","\n","\\r","\\n","\\r\\n"), "", $val));
					}
				}

				$trans_id = $list['TRANS_ID'];
				$json = json_encode($newlist, JSON_UNESCAPED_UNICODE);

			} else {
				$trans_id = $list['TRANS_ID'];
				$json = json_encode($list, JSON_UNESCAPED_UNICODE);
			}

			/* check trans_id in log db */
			$result = $this->db->where(['trans_id'=>$trans_id,'status'=>0])->get('transactions')->result_array();
			/* check trans_id in image db */
			$chkimg = $this->db->select('ID')->where(['trans_id'=>$trans_id])->get('transactions_images')->result_array();
			/* Insert transaction log to db */			
			if (count($result) > 0) {
				$fields = ['data'=>$json];
				$this->db->set('update_date', 'NOW()', false);
				$this->db->update("transactions", $fields, ["id"=>$result[0]['id']]);
			} else {
				$fields = ["trans_id" => $trans_id,	"type" => $type,	"data" => $json];
				$this->db->set('created_date', 'NOW()', FALSE);
				$this->db->insert("transactions", $fields);
			}

			if ($type=='new' || $type=='update') {
				/* Insert image data */
				$fields = [
					"TRANS_ID" => $trans_id,
					"PIC_FILE" => $images["PIC_FILE"],
					"PIC_FILE2" => $images["PIC_FILE2"],
					"PIC_FILE3" => $images["PIC_FILE3"],
					"PIC_FILE4" => $images["PIC_FILE4"],
					"PIC_FILE5" => $images["PIC_FILE5"],
				];

				if (count($chkimg) > 0) {
					$this->db->update("transactions_images", $fields, ["TRANS_ID"=>$trans_id]);
				} else {
					$this->db->set('CREATED_DATE', 'NOW()', false);
					$this->db->insert("transactions_images", $fields);
				}          
			}			
		}

		function updateTransLogs($list, $status){
			$this->db->set('update_date', 'NOW()', FALSE);
			$this->db->update("transactions", ['status'=>$status], ['id'=>$list['id']]);
		}
		
		function getTransactionsQueue($type, $status){
			$this->db->select('id,trans_id,type');
			$this->db->where(['type'=>$type,'status'=>$status]);
			return $this->db->get('transactions')->result_array();
		}

		function getCountQueueLog($type = false){	
			if ($type) {
				$this->db->where($type[0], $type[1]);
			}
			$this->db->where('DATE(sender_time)',date('Y-m-d'));
			return $this->db->count_all_results('queue');
		}

		function getAllQueueLog(){
			$this->db->order_by('id ASC');
			return $this->db->get('queue')->result_array();
		}

		function addQueueLog($params){
			$fields = [
				'getid'=>$params['id'],
				'type'=>$params['type'],
				'trans_id'=>$params['trans_id'],
				'status'=>'Waiting'
			];

			$this->db->set('sender_time', 'NOW()', FALSE);
			$this->db->insert("queue", $fields);
		}

		function updateQueueLog($params, $state){
			if($state == 'Processing'){
				$this->db->set('recived_time', 'NOW()', FALSE);
			} else {
				$this->db->set('success_time', 'NOW()', FALSE);
			}

			$this->db->where('status !=', 'End');

			$this->db->update("queue", ['status'=>$state], ['trans_id'=>$params['trans_id']]);
		}

		function insertApiLog($list, $status, $msg=null) {
			$fields = [
				"trans_id" => $list['trans_id'],
				"type" => $list['type'],
				"log_data" => $msg,
				"status" => $status
			];

			$this->db->set('log_date', 'NOW()', FALSE);
			$this->db->insert("mage", $fields);
			return $this->db->insert_id();
		}

		function getAllApiLog($where = false) {
			if($where)
				$this->db->where($where[0], $where[1]);
			$this->db->order_by('log_date DESC');
			return $this->db->get('mage')->result_array();
		}

		function debugg(){
			$delete = [];
			//$tables = array('mage', 'transactions');
			//$this->db->where_in('trans_id', $delete);
			//$this->db->delete($tables);
		}

	}
