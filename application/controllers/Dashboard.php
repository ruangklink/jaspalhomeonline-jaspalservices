<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

 	public function __construct() {
		parent::__construct();
		/* load libraries */
		$this->load->library(['rabbitmqclass', 'jaspalservices']);		
		/* load helper */
		// $this->load->helper(['form']);
		/* load model */
		$this->load->model(['webservices_model']);
	}

	public function index()	{
		$data['cnt_all'] = $this->webservices_model->getCountQueueLog();
		$data['cnt_wait'] = $this->webservices_model->getCountQueueLog(['status','Waiting']);
		$data['cnt_process'] = $this->webservices_model->getCountQueueLog(['status','Processing']);
		$data['cnt_end'] = $this->webservices_model->getCountQueueLog(['status','End']);
		$this->load->view('dashboard/home', $data);
	}
	public function connection()	{
		$data['rows'] = $this->webservices_model->getAllConnectLog();
		$this->load->view('dashboard/connection', $data);
	}
	public function queue()	{
		$data['rows'] = $this->webservices_model->getAllQueueLog();
		$this->load->view('dashboard/queue', $data);
	}
	public function transaction()	{
		$data['rows'] = $this->webservices_model->getAllTransLog();
		$this->load->view('dashboard/transaction', $data);
	}
	public function api()	{
		$data['rows'] = $this->webservices_model->getAllApiLog();
		$this->load->view('dashboard/api', $data);
	}
	public function daily()	{
		$data = [];
		$this->load->view('dashboard/daily', $data);
	}

}
