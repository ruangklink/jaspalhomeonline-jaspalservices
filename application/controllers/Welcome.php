<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	public $mageAuth;
	public $mageLogin;
	public $targetMethod = [
		'new'=>'insertProduct',
		'update'=>'updateProduct',
		'stock'=>'updateInventory',
		'store'=>'updateStoreLocator',
	];

 	public function __construct() {
		parent::__construct();
		/* load libraries */
		$this->load->library(['rabbitmqclass', 'jaspalservices', 'magentoservices', 'mageclass']);		
		/* load model */
		$this->load->model(['webservices_model']);
	}

	public function index()	{
		echo '<h3>Jaspal web services</h3><br>';		
		die();
  }

	public function trigger()	{
		/** connect jaspal webservice */
		// $stop_requested = false;
    // pcntl_signal( SIGTERM, function() {
    //   global $stop_requested;
		// 	$stop_requested = true;
		// 	echo 'p stop => true \\r\\n';
		// });

		// echo 'p stop =>'.$stop_requested.'\\r\\n';
		// echo date('Y-m-d H:i:s').' => run \\r\\n';
		// exit(0);
		
		$response = $this->connectJaspalService();
		if($response > 0) {
			// get total record
			$total = $this->webservices_model->getConnectLog($response);

			if(intVal($total[0]['total_add']) == 0 && intVal($total[0]['total_update']) == 0 && intVal($total[0]['total_stock']) == 0 && intVal($total[0]['total_store']) == 0){
				echo date('Y-m-d H:i:s')." => Exit process getMainjob all data = 0 pcs. \\\\r\\\\n";
				exit(0);
			} else {
				if (intVal($total[0]['total_add']) > 0)
					$this->getProductInfoLoopFunction('new', 'GetNewProdInfoApi');
				if (intVal($total[0]['total_update']) > 0)
					$this->getProductInfoLoopFunction('update', 'GetUpdateProdInfoApi');
				if (intVal($total[0]['total_stock']) > 0)
					$this->getProductInfoLoopFunction('stock', 'GetUpdateStockApi');
				if (intVal($total[0]['total_store']) > 0)
					$this->getProductInfoLoopFunction('store', 'GetUpdateStoreApi');
		
				// get data from transactions, then add to queue
				$new_data = $this->webservices_model->getTransactionsQueue('new', 0);
				$update_data = $this->webservices_model->getTransactionsQueue('update', 0);
				$stock_data = $this->webservices_model->getTransactionsQueue('stock', 0);
				$store_data = $this->webservices_model->getTransactionsQueue('store', 0);

				// connect rabbitmq server, then add queue
				$rbq = new Rabbitmqclass();
				$qConnection = $rbq->connection();
				$qChannel = $qConnection->channel();
				$qChannel->queue_declare('mage_trans_queue', false, false, false, false);

				if (!empty($new_data)) {
					foreach ($new_data as $row) {
						$msg = $rbq->addAMQPMessage(json_encode($row), array('delivery_mode' => 2));
						$qChannel->basic_publish($msg, '', 'mage_trans_queue');
						$this->webservices_model->addQueueLog($row);
					}
				}
				if (!empty($update_data)) {
					foreach ($update_data as $row) {
						$msg = $rbq->addAMQPMessage(json_encode($row), array('delivery_mode' => 2));
						$qChannel->basic_publish($msg, '', 'mage_trans_queue');
						$this->webservices_model->addQueueLog($row);
					}
				}
				if (!empty($stock_data)) {
					foreach ($stock_data as $row) {
						$msg = $rbq->addAMQPMessage(json_encode($row), array('delivery_mode' => 2));
						$qChannel->basic_publish($msg, '', 'mage_trans_queue');
						$this->webservices_model->addQueueLog($row);
					}
				}
				if (!empty($store_data)) {
					foreach ($store_data as $row) {
						$msg = $rbq->addAMQPMessage(json_encode($row), array('delivery_mode' => 2));
						$qChannel->basic_publish($msg, '', 'mage_trans_queue');
						$this->webservices_model->addQueueLog($row);
					}
				}

				$qChannel->close();
				$qConnection->close();
				
				$data = ['connection'=>'CONNECT-SUCCESS', 'queue'=>'ADD-QUEUE-SUCCESS'];
			}
	
		} else {
			$data = ['connection'=>'CONNECT-ERROR', 'queue'=>'ADD-QUEUE-ERROR'];
		}

		echo date('Y-m-d H:i:s').'Jaspal web services Processing...\\\\r\\\\n';
		echo date('Y-m-d H:i:s').'Connection: '.$data['connection'].'\\\\r\\\\n';
		echo date('Y-m-d H:i:s').'Queue: '.$data['queue'].'\\\\r\\\\n';
		sleep(10);
		exit(0);
	}

	public function test_exit(){
		$params = [
			'MessageCode'=>'CONNECT-SOAP-ERROR',
			'MessageDesc'=>'TEST',
			'TotalRecordAdd'=>0,
			'TotalRecordUpdate'=>0,
			'TotalRecordStock'=>0,
			'TotalRecordStore'=>0,
		];
		// insert connection log
		$log = $this->webservices_model->insertConnectLogTest('ERROR', $params);
		$this->load->view('welcome/index');
		exit(0);		

		// set_exception_handler(function() {
		// 	exit(56);			
		// });
		// throw new Exception('Exit process');
	}

	public function execute($statusCode) { 
    echo $statusCode; 
	} 


	public function debugg(){
		die('End [debugg]');
		// $this->webservices_model->debugg();
		// die('debug');
		// $mage = new Mageclass();
		// $mage->__debugg();

		// add store value by mage
		$store_data = $this->webservices_model->getTransactionsQueue('store', 0);
		foreach ($store_data as $k => $v) {
			//var_dump($v);
			$fields = ['getid'=>$v['id'], 'trans_id'=>$v['trans_id'], 'type'=>$v['type']];
			$data = $this->webservices_model->getTransLog($fields);
			$mage = new Mageclass();
			$res = $mage->__debugg($data[0]);
			echo $res;
			$this->webservices_model->updateTransLogs($data[0], 1);
			if($k == 415)
				die('end');
		}
		die('End [debugg]');
	}

	public function receiver_queue() {	
		// connect rabbitmq server, then add queue
		$rbq = new Rabbitmqclass();
		$qConnection = $rbq->connection();
		$qChannel = $qConnection->channel();
		$qChannel->queue_declare('mage_trans_queue', false, false, false, false);

		// callback function
    $callback = function($msg) { 
			// get data from queue
  		$response = json_decode($msg->body, true);			
			$id = ! empty($response['id']) ? $response['id'] : '';
			$trans_id = ! empty($response['trans_id']) ? $response['trans_id'] : '';
			$type = ! empty($response['type']) ? $response['type'] : '';
			$fields = ['getid'=>$id, 'trans_id'=>$trans_id, 'type'=>$type];
			// write log before process
			$this->webservices_model->updateQueueLog($fields, 'Processing');
			// process to magento api
			if($trans_id != ''){
				// get transaction data from database
				$data = $this->webservices_model->getTransLog($fields);
				if (!empty($data)) {
					$result = $this->manageMagentoApi($data[0], $fields['type']);	

					if($result['qStatus']){
						$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
						$this->webservices_model->updateQueueLog($fields, 'End');
					}					
				} else {
					$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
					$this->webservices_model->updateQueueLog($fields, 'End');						
				}				
			} else {
				// remove queue
				$msg->delivery_info['channel']->basic_ack($msg->delivery_info['delivery_tag']);
			}		
    };
    
    $qChannel->basic_qos(null, 1, null);
    $qChannel->basic_consume('mage_trans_queue', '', false, false, false, false, $callback);
    
    while($qChannel->is_consuming()) {
      $qChannel->wait();
    }

    $qChannel->close();
    $qConnection->close();    
	}

	public function auto_cancel_order(){
		$mage = new Mageclass();
		$mage->_applyAutoCancelOrder();
		die('End [auto_cancel_order]');
	}

	public function auto_apply_rule(){
		$mage = new Mageclass();
		$mage->_applyRule();
		die('End [auto_apply_rule]');
	}

	public function manageMagentoApi($product, $type){
		// Update transaction log
		$this->webservices_model->updateTransLogs($product, 1);
		$mConnect = $this->connectMagentoServices();			
		// Check authentification
		if($mConnect){
			// Authentification Success, Then add product to e-comerce website, Call soap api
			$service = $this->mageAuth->execute($this->targetMethod[$type], $product);
			$json = (array) json_decode($service, true);
			// insert api log to db
			$result = $this->webservices_model->insertApiLog($product , $json['transaction']['status'], $service);
			// send status to jaspal service
			$this->postUpdateSendFlagApiUpdate($product['trans_id'], $json['transaction']['status'], $json['transaction']['msg']);

			$queue_status = ['qStatus'=>true];
		} else {
			// Authentification Error, Then Add log to database
			$service = [
				'auth'=>['status'=>'UN-COMPLETED', 'msg'=>'Access denied.'],
				'transaction'=>['trans_id'=>$product['trans_id'], 'status'=>'UN-COMPLETED', 'msg'=>'Access denied.']
			];
			// insert api log to db
			$result = $this->webservices_model->insertApiLog($product , 'UPDATE-UN-COMPLETED', json_encode($service));
			// send status to jaspal service
			$this->postUpdateSendFlagApiUpdate($product['trans_id'], 'UPDATE-UN-COMPLETED', 'Access denied.');

			$queue_status = ['qStatus'=>true];
		}

		return $queue_status;
	}

	public function connectMagentoServices(){		
		if(!$this->mageAuth && !$this->mageLogin){
			$this->mageAuth = new Magentoservices();
			$this->mageLogin = $this->mageAuth->login();		
			return $this->mageLogin;
		} else {
			return $this->mageLogin;
		}		
	}

	function connectJaspalService(){
		// connect 
		$jaspal_service = new Jaspalservices();
		$jsoap = $jaspal_service->connectSoap();

		// Clear database temp
		$this->webservices_model->clearTemporary();

		if($jsoap->getError()){
			// connect soap error, 
			$params = [
				'MessageCode'=>'CONNECT-SOAP-ERROR',
				'MessageDesc'=>$jsoap->getError(),
				'TotalRecordAdd'=>0,
				'TotalRecordUpdate'=>0,
				'TotalRecordStock'=>0,
				'TotalRecordStore'=>0,
			];
			// insert connection log
			$log = $this->webservices_model->insertConnectLog('ERROR', $params);
			// Apply Rule
			$jaspal_service->_applyRule();
			// Apply Auto Cancel Order
			$jaspal_service->_applyAutoCancelOrder();
			
			$data = ['connection'=>'CONNECT-SOAP-ERROR'];
			$log = 0;
		} else {
			// Call Service ==>> [ MainJobProcDataApi ]
			$result = $jaspal_service->MainJobProcDataApi();
			
			if ($result[0]['MessageCode']=='COMPLETED') {
				// insert log data
				$log = $this->webservices_model->insertConnectLog('SUCCESS', $result[0]);
				// insert temp data
				$this->webservices_model->addTemporary('new', intVal($result[0]['TotalRecordAdd']), 0);
				$this->webservices_model->addTemporary('update', intVal($result[0]['TotalRecordUpdate']), 0);
				$this->webservices_model->addTemporary('stock', intVal($result[0]['TotalRecordStock']), 0);
				$this->webservices_model->addTemporary('store', intVal($result[0]['TotalRecordStore']), 0);

			} else {
				$params = [
					'MessageCode'=>$result[0]['MessageCode'],
					'MessageDesc'=>$result[0]['MessageDesc'],
					'TotalRecordAdd'=>0,
					'TotalRecordUpdate'=>0,
					'TotalRecordStock'=>0,
					'TotalRecordStore'=>0,
				];
				
				$log = $this->webservices_model->insertConnectLog('ERROR', $params);
			}
		}

		return $log;
	}

	function getProductInfoLoopFunction($type, $service){		
		// Loop get service data
		$loop = FALSE;
		do {
			// get service get data complete status
			$tempStep4 = $this->webservices_model->getTemporary($type , 4);
			// delete service get data complete record
			if(count($tempStep4)>0){
				$this->webservices_model->deleteTemporary($tempStep4[0]['id']);
				sleep(1);
				continue;
			} else {
				// check service get data complete but not send flag
				$tempStep3 = $this->webservices_model->getTemporary($type , 3);
				if(count($tempStep3)>0){
					sleep(1);
					continue;
				} else {
					// check service getProductInfoApi complete */
					$tempStep2 = $this->webservices_model->getTemporary($type , 2);
					if(count($tempStep2)>0){					
						// if complete but not send flag, then call service postUpdateSendFlagApi
						$this->webservices_model->updateTemporary($tempStep2[0]['id'], 3, $tempStep2[0]['trans_id']);
						$this->postUpdateSendFlagApi($tempStep2[0]['trans_id'], 'SEND-COMPLETED', '', $tempStep2[0]['id']);
						sleep(1);
						continue;
					} else {
						// check status getProductInfoApi is running
						$tempStep1 = $this->webservices_model->getTemporary($type , 1);
						if(count($tempStep1)>0){
							// have process running
							sleep(1);
							continue;
						} else {
							// if no have getProductInfoApi process running, then call next record
							$tempStep0 = $this->webservices_model->getTemporary($type , 0);
							if(count($tempStep0)>0){					
								// run getProductInfoApi next record
								$this->webservices_model->updateTemporary($tempStep0[0]['id'] , 1);
								$this->getProductInfoApi($tempStep0[0]['id'], $type, $service);
								sleep(1);
								continue;
							}
						}
					}
				}
			}

			// Update loop
			$tempStep5 = $this->webservices_model->getTemporary($type , 5);
			if(count($tempStep5)==0){
				$loop = TRUE;
			}

		} while(!$loop);
	}

	function getProductInfoApi($id, $type, $service){
		$jaspal_service = new Jaspalservices();
		$jsoap = $jaspal_service->connectSoap();

		if ($jsoap->getError()) {
			// connect soap error
		} else {
			// Call Service
			$result = $jaspal_service->GetProdInfoApi($service, ['TotalRecord'=>1]);
			if (count($result)) {
				// update response to temp db
				foreach ($result as $_result) {
					// Save transaction log
					$this->webservices_model->insertTransLog($_result, $type);
					$this->webservices_model->updateTemporary($id, 2, $_result['TRANS_ID']);
				}
			}
		}
	}

	function postUpdateSendFlagApi($trans_id, $status, $errlog, $id){
		$jaspal_service = new Jaspalservices();
		$jsoap = $jaspal_service->connectSoap();
		if ($jsoap->getError()) {
			// connect soap error
		} else {
			$params = [
				'TransactionId'=>$trans_id,
				'flagStatus'=>$status,
				'ErrMsg'=>$errlog
			];
			$result = $jaspal_service->PostUpdateSendFlagApi($params);
			if (count($result)) {
				if ($result[0]['MessageCode']=='COMPLETED') {
					$this->webservices_model->updateTemporary($id, 4, $trans_id);
				}
			}
    }
	}

	function postUpdateSendFlagApiUpdate($trans_id, $status, $errlog){
		$jaspal_service = new Jaspalservices();
		$jsoap = $jaspal_service->connectSoap();
		if ($jsoap->getError()) {
			// connect soap error
		} else {
			$params = [
				'TransactionId'=>$trans_id,
				'flagStatus'=>$status,
				'ErrMsg'=>$errlog
			];

			$result = $jaspal_service->PostUpdateSendFlagApi($params);
    }
	}

}
