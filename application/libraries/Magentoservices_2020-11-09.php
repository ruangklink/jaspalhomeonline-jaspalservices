<?php
/*
 * Narenrit Hadsadintorn
 *
 * The first PHP Library to support jaspal webservices to nagento soap api
 */
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(FCPATH.'vendor/autoload.php');

// tew change status by qty & store qty
require_once(str_replace('/JaspalServices/', '/', FCPATH) . 'app/Mage.php');

class Magentoservices {
	private $ci;
	private $_helper;
	private $_serviceV1;
	private $_sessionV1;
	private $_urlV1;
	private $_service;
	private $_session;
	private $_url;
	private $_login = false;
	private $_apiUser;
	private $_apiKey;

	/* Default Variable */
	private $viewDF; // Store View ADMIN / DEFAULT Id
	private $viewEN; // Store View EN Id
	private $viewTH; // Store View TH Id
	private $setDF; // default Product Set ID
	private $webDF; // Jaspal Wesite ID

	public function __construct()
	{
		$this->ci =& get_instance();	
		// init soap variable
		$this->_url = $this->ci->config->item('soap_service_v2_url', 'webservices');
		$this->_service = new SoapClient($this->_url, 
			array('trace'=>true,'keep_alive'=>false,'cache_wsdl'=>WSDL_CACHE_NONE)
		);
		$this->_urlV1 = $this->ci->config->item('soap_service_v1_url', 'webservices');
		$this->_serviceV1 = new SoapClient($this->_urlV1, 
			array('trace'=>true,'keep_alive'=>false,'cache_wsdl'=>WSDL_CACHE_NONE)
		);
		// init magento api auth
		$this->_apiUser = $this->ci->config->item('api_user', 'webservices');
		$this->_apiKey = $this->ci->config->item('api_key', 'webservices');
		// init magento web store config
		$this->viewDF = $this->ci->config->item('view_df', 'webservices'); 
		$this->viewEN = $this->ci->config->item('view_en', 'webservices');
		$this->viewTH = $this->ci->config->item('view_th', 'webservices');
		$this->setDF = $this->ci->config->item('set_df', 'webservices');
		$this->webDF = $this->ci->config->item('web_df', 'webservices');

		$params = array(
			'viewDF'=>$this->viewDF,
			'viewEN'=>$this->viewEN,
			'viewTH'=>$this->viewTH,
			'setDF'=>$this->setDF,
			'webDF'=>$this->webDF
		);

		// load helper
		$this->ci->load->helper(['variable']);
		$this->_helper = new VariableHelper($params);
	}

	/**
	 * excute function
	 * @param  string $target
	 * @param  array $lists
	 * @return string
	 */
	public function execute($target, $lists) {
		// Check Login Success or Denied
		if($this->_login){
			// Call Function
			$result = $this->{$target}($lists);

			return json_encode($result, JSON_UNESCAPED_UNICODE);
		} else {
			$result = array(
			'auth'=>array('status'=>'UN-COMPLETED', 'msg'=>'Access denied.'),
			'transaction'=>array('trans_id'=>$lists['TRANS_ID'], 'status'=>'UPDATE-UN-COMPLETED', 'msg'=>'Access denied.')
				);

			return json_encode($result, JSON_UNESCAPED_UNICODE);
		}
	}

	/**
	 * Authentification Api
	 * If somestuff requires api authentification, then get a session token
	 * @return boolean
	 */
	public function login(){
		try {
			// Login Success
				$this->_session = $this->_service->login($this->_apiUser, $this->_apiKey);
				$this->_sessionV1 = $this->_serviceV1->login($this->_apiUser, $this->_apiKey);
				$this->_login = true;
				return true;
		} catch (SoapFault $e) {
			// Login Internal Error. Please see log for details.
			return false;
		}
	}

	/**
	 * insert product info api
	 * @param  array $lists
	 * @return array
	 */
	function insertProduct($lists){
		if(!empty($lists)){
			// init product data json to array
			$list = $this->_helper->initProdInfo($lists);
			if($list['type']=='simple'){
				// insert simple product
				$result = $this->insertSimpleProduct($lists['trans_id'], $list);
			} else {
				// insert configurable product
				$result = $this->insertConfigProduct($lists['trans_id'], $list);
			}
		} else {
			$result = array(
				'auth'=>array('status'=>'COMPLETED', 'msg'=>''),
				'transaction'=>array('trans_id'=>$lists['trans_id'], 'status'=>'UPDATE-UN-COMPLETED', 'msg'=>'No product data.')
			);
		}

		return $result;
	}

	/**
	 * update product info api
	 * @param  array $lists
	 * @return array
	 */
	function updateProduct($lists){
		if(!empty($lists)){
			// init product data json to array
			$list = $this->_helper->initProdInfo($lists);

			if($list['type']=='simple'){
				// update simple product
				$result = $this->updateSimpleProduct($lists['trans_id'], $list);
			} else {
				// update configurable product
				$result = $this->updateConfigProduct($lists['trans_id'], $list);
			}
		} else {
			$result = array(
				'auth'=>array('status'=>'COMPLETED', 'msg'=>''),
				'transaction'=>array('trans_id'=>$lists['trans_id'], 'status'=>'UPDATE-UN-COMPLETED', 'msg'=>'No product data.')
			);
		}

		return $result;
	}

	/**
	 * update inventory info api
	 * @param  array $lists
	 * @return array
	 */
	function updateInventory($lists){
		if(!empty($lists)){
			// init product data json to array
			$list = $this->_helper->initProdInfo($lists);
			// update inventory
			$result = $this->updateInventoryApi($lists['trans_id'], $list);
		} else {
			$result = array(
				'auth'=>array('status'=>'COMPLETED', 'msg'=>''),
				'transaction'=>array('trans_id'=>$lists['trans_id'], 'status'=>'UPDATE-UN-COMPLETED', 'msg'=>'No inventory data.')
			);
		}

		return $result;
	}
			
	/**
	 * update inventory info api
	 * @param  array $lists
	 * @return array
	 */
	function updateStoreLocator($lists){
		if(!empty($lists)){
			// init product data json to array
			$list = $this->_helper->initProdInfo($lists);
			// update store
			$result = $this->updateStoreLocatorApi($lists['trans_id'], $list);
		} else {
			$result = array(
				'auth'=>array('status'=>'COMPLETED', 'msg'=>''),
				'transaction'=>array('trans_id'=>$lists['trans_id'], 'status'=>'UPDATE-UN-COMPLETED', 'msg'=>'No store data.')
			);
		}

		return $result;
	}

	// ================ Inventory Function ================ //
	/**
	 * updateInventoryApi
	 * @param  string $trans_id
	 * @param  array $list
	 * @return array
	 */
	function updateInventoryApi($trans_id, $list){
			// Check Product sku in store view
		$msg = '';
		try {
			$inventory = $this->_service->catalogInventoryStockItemList($this->_session, array($list['prod_info']['sku']));
		} catch (SoapFault $e) {
			$msg = $e->faultstring;
			$inventory = array();
		}

		if(!empty($inventory)){
			// Update inventory data
			foreach ($inventory as $key => $inv) {
				// Init inventory parameter.
				$qty = ((int) $list['prod_inv']['qty'] + (int) $inv->qty);
				//$qty = (int) $list['prod_inv']['qty'];
				$params = array('qty'=>$qty, 'is_in_stock'=>$list['prod_inv']['is_in_stock']);
				// Update inventory to store view
				try {
					// $stock = $this->_service->catalogInventoryStockItemUpdate($this->_session, $inv->product_id, $params);

					// tew change status by qty & store qty
					$params = $this->checkUpdateProductStatus($list['prod_info']['sku'], array('stock_data' => $params), 'stock');
					// tew change status by qty & store qty : แก้การส่งค่า เป็น $params    
					$stock = $this->_service->catalogProductUpdate($this->_session, $inv->product_id, $params, $this->viewDF);

					// [OLD] แก้การส่งค่า เป็น $params
					//$stock = $this->_service->catalogProductUpdate($this->_session, $inv->product_id, array('stock_data'=>$params), $this->viewDF);

				} catch (SoapFault $e) {
					$msg = $e->faultstring;
					// $stock = 0;
				}
			}

			if($stock){
				// update inventory to all store view success.
				$result = array(
					'auth'=>array('status'=>'COMPLETED', 'msg'=>''),
					'transaction'=>array('trans_id'=>$trans_id, 'status'=>'UPDATE-COMPLETED', 'msg'=>'Update Inventory Success.')
				);
			} else {
				// Can't update inventory to store view.
				$result = array(
					'auth'=>array('status'=>'COMPLETED', 'msg'=>''),
					'transaction'=>array('trans_id'=>$trans_id, 'status'=>'UPDATE-UN-COMPLETED', 'msg'=>'Can not update inventory, Exception : '.$msg)
				);
			}
		} else {
			// Can't update inventory because no product sku in database.

			// tew change status by qty & store qty : แก้ status UPDATE-UN-COMPLETED เป็น   UPDATE-COMPLETED  เพิ่ม sku ใน  msg
			$result = array(
				'auth'=>array('status'=>'COMPLETED', 'msg'=>''),
				'transaction'=>array('trans_id'=>$trans_id, 'status'=> 'UPDATE-UN-COMPLETED', 'msg'=>'Can not update inventory, not found product sku: ['.$list['prod_info']['sku']. '] in database')
			);

			// [OLD] tew change status by qty & store qty  : แก้ status UPDATE-UN-COMPLETED เป็น   UPDATE-COMPLETED  เพิ่ม sku ใน  msg
			// $result = array(
			// 	'auth'=>array('status'=>'COMPLETED', 'msg'=>''),
			// 	'transaction'=>array('trans_id'=>$trans_id, 'status'=>'UPDATE-UN-COMPLETED', 'msg'=>'Can not update inventory, No product sku in db')
			// );

		}

		return $result;
	}
	// ================ Store Function ================ //
	/**
	 * updateStoreLocatorApi
	 * @param  string $trans_id
	 * @param  array $list
	 * @return array
	 */
	function updateStoreLocatorApi($trans_id, $list){
		// Init Data
		$params_option = $this->initOptionStoreValue($list['prod_attr']);
		$params_attr = $this->_helper->initProductAttrStoreValue($list, $params_option, $this->viewDF);
		$params = $this->_helper->initProductStoreValue($params_attr);
	
		// check update master / simple
		$productUpdate = null;
		$msg = '';
		// Update simple Product data to Storview	
		try {

			// tew change status by qty & store qty
			$params = $this->checkUpdateProductStatus($list['prod_info']['sku'], $params, 'store');

			$productUpdate = $this->_service->catalogProductUpdate($this->_session, $list['prod_info']['sku'], $params, $this->viewDF);
		} catch (SoapFault $e) {
			$msg = $e->faultstring;
		}		

		$inCatagoryReady = false;
		try {
			$category = $this->_service->catalogProductInfo($this->_session, $list['prod_info']['sku']);				
			if(!empty($category)){
				if(isset($category->category_ids)){
					foreach ($category->category_ids as $category_ids) {
						if(intVal($category_ids) == intVal($this->ci->config->item('find_in_shop_id', 'webservices'))){
							$inCatagoryReady = true;
						}
					}				
				}
			}
		} catch (SoapFault $e) {}	
		
		if(!$inCatagoryReady){
			try {
				$category = $this->_service->catalogCategoryAssignProduct($this->_session, $this->ci->config->item('find_in_shop_id', 'webservices'), $list['prod_info']['sku']);			
			} catch (SoapFault $e) {}	
		}			
		
		if($productUpdate){
			// Update product to all store view success.
			$result = array(
				'auth'=>array('status'=>'COMPLETED', 'msg'=>''),
				'transaction'=>array('trans_id'=>$trans_id, 'status'=>'UPDATE-COMPLETED', 'msg'=>'Update To All StoreView Success.')
			);
		} else {
			// Can't update product to all store view
			$result = array(
				'auth'=>array('status'=>'COMPLETED', 'msg'=>''),
				'transaction'=>array('trans_id'=>$trans_id, 'status'=>'UPDATE-UN-COMPLETED', 'msg'=>'Can not update product store, Exception : '.$msg)
			);
		}

		return $result;
	}
	// ================ Simple Product Function ================ //
	/**
	 * Insert Simple Product
	 * @param  array $list
	 * @param  integer $invisable
	 * @return array
	 */
	function insertSimpleProduct($trans_id, $list, $invisable=4){
		// Init Data
		$productId = 0;
		$params_option = $this->initOptionValue($list['prod_attr']);
		$params_attr = $this->_helper->initProductAttrValue($list, $params_option, $this->viewDF);
		$params_inv = $this->_helper->initProductInventoryValue($list);
		$params = $this->_helper->initProductValue($list, $params_attr, $params_inv, $invisable);

		// Add Product data to Storview
		$msg = '';
		try {
			// Insert simple product to view default/en
			$productId = $this->_service
			->catalogProductCreate($this->_session, 'simple', $this->setDF, $list['prod_info']['sku'], $params, $this->viewDF);
		} catch (SoapFault $e) {
			$msg = $e->faultstring;
		}

		if($productId > 0){
			// Add Media Gallery
			$this->insertApiImagesGallery($productId, $list['prod_img']);
			// Init TH Variable
			$params = $this->_helper->initProductValueTH($list);
			// Update product data to Storeview TH
			try {
				$productUpdate = $this->_service->catalogProductUpdate($this->_session, $productId, $params, $this->viewTH);
			} catch (SoapFault $e) {
				$msg = $e->faultstring;
			}

			if($productUpdate){
				// Insert product to all store view success.
				$result = array(
					'auth'=>array('status'=>'COMPLETED', 'msg'=>''),
					'transaction'=>array('trans_id'=>$trans_id, 'status'=>'UPDATE-COMPLETED', 'msg'=>'Insert To All StoreView Success.')
				);
			} else {
				// Can't insert product to store view th
				$result = array(
					'auth'=>array('status'=>'COMPLETED', 'msg'=>''),
					'transaction'=>array('trans_id'=>$trans_id, 'status'=>'UPDATE-COMPLETED', 'msg'=>'Insert ViewEN Success, ViewTH Not Success, Exception : '.$msg)
				);
			}
		} else {
			// Can't insert product to all store view
			$result = array(
				'auth'=>array('status'=>'COMPLETED', 'msg'=>''),
				'transaction'=>array('trans_id'=>$trans_id, 'status'=>'UPDATE-UN-COMPLETED', 'msg'=>'Can not insert product, Exception : '.$msg)
			);
		}

		return $result;
	}

	/**
	 * Update Simple Product
	 * @param  array $list
	 * @param  integer $invisable
	 * @return array
	 */
	function updateSimpleProduct($trans_id, $list, $invisable=4){
		// Init Data
		$params_option = $this->initOptionValue($list['prod_attr']);
		$params_attr = $this->_helper->initProductAttrValue($list, $params_option, $this->viewDF);
		//$params_inv = array(); // $this->_helper->initProductInventoryValue($list);
		$update_qty = $this->initInventoryCaseProductUpdate($list['prod_info']['sku']);
		$params_inv = $this->_helper->initProductInventoryValue($list, $update_qty, true);
		$params = $this->_helper->initProductValue($list, $params_attr, $params_inv, $invisable);

		// check update master / simple
		$productUpdate = null;
		$msg = '';

		if(strlen($list['prod_info']['sku']) == 8){
			// Update Configurable Product data to Storview			
			// Have master product ready, then check child product
			$associated = array();
			$master = $this->getApiProductInfo($list['prod_info']['sku'], 1);
			if(!empty($master["associated_skus"])){
				foreach ($master["associated_skus"] as $skus) {
					array_push($associated, $skus);
				}

				$productUpdate = $this->updateMasterConfigProductInfo($trans_id, $master["product_id"], $list, $associated);
			} else {
				$productUpdate = false;
			}				
		} else {
			// Update simple Product data to Storview			
			if($list['type'] == 'configurable'){
				$params['additional_attributes'] = array('single_data'=>$params_attr);
			}

			try {
					// tew change status by qty & store qty
					$params = $this->checkUpdateProductStatus($list['prod_info']['sku'], $params, 'update');
					$productUpdate = $this->_service->catalogProductUpdate($this->_session, $list['prod_info']['sku'], $params, $this->viewDF);
			} catch (SoapFault $e) {
				$msg = $e->faultstring;
			}

			// Update Media Gallery
			$this->updateApiImagesGallery($list['prod_info']['sku'], $list['prod_img']);
			// Init TH Variable
			$params = $this->_helper->initProductValueTH($list);
			// Update product data to Storeview TH
			try {
				$productUpdate = $this->_service->catalogProductUpdate($this->_session, $list['prod_info']['sku'], $params, $this->viewTH);
			} catch (SoapFault $e) {
				$msg = $e->faultstring;
			}

			// update to master configurable
			if($list['type'] == 'configurable'){
				$the_sku = substr($list['prod_info']['sku'], 0, 8);
				$getmaster = $this->getApiProductInfo($the_sku, 1);
				$associated_skus = array();
				if(!empty($getmaster["associated_skus"])){
					foreach ($getmaster["associated_skus"] as $skus) {
						array_push($associated_skus, $skus);
					}
				}

				if (!in_array($list['prod_info']['sku'], $associated_skus)) {
					array_push($associated_skus, $list['prod_info']['sku']);
				}

				// Third update master configurable product
				if($productUpdate){
					$master = $this->updateMasterConfigProduct($trans_id, $the_sku, $associated_skus);
				}
			}
		}			

		if($productUpdate){
			// Update product to all store view success.
			$result = array(
				'auth'=>array('status'=>'COMPLETED', 'msg'=>''),
				'transaction'=>array('trans_id'=>$trans_id, 'status'=>'UPDATE-COMPLETED', 'msg'=>'Update To All StoreView Success.')
				);
		} else {
			// Can't update product to all store view
			$result = array(
				'auth'=>array('status'=>'COMPLETED', 'msg'=>''),
				'transaction'=>array('trans_id'=>$trans_id, 'status'=>'UPDATE-UN-COMPLETED', 'msg'=>'Can not update product, Exception : '.$msg)
					);
		}

		return $result;
	}

	// ================ Configurable Product Function ================ //
	/**
	 * Insert Configurable Product
	 * @param  string $trans_id
	 * @param  array $list
	 * @return array
	 */
	function insertConfigProduct($trans_id, $list){
		// Substring SKU 8 Digit To master configurable product
		$sku = substr($list['prod_info']['sku'], 0, 8);
		// Check SKU ready in DB
		try {
			$chkMaster = $this->_service->catalogProductInfo($this->_session, $sku);
		} catch (SoapFault $e) {}

		if(!isset($chkMaster)){
			// No have master product, Then add child product to simple product first
			$child = $this->insertSimpleProduct($trans_id, $list, 1);
			// Second add master product to configurable Product
			if($child['transaction']['status']=='UPDATE-COMPLETED'){
				$master = $this->addMasterConfigProduct($trans_id, $sku, $list, array($list['prod_info']['sku']));
			}

			// return value
			$result = $child;

		} else {
			// Have master product ready, then check child product
			try {
				$chkChild = $this->_service->catalogProductInfo($this->_session, $list['prod_info']['sku']);
			} catch (SoapFault $e) {}

			if(!isset($chkChild)){
				// No have child product, Then add child product first
				$child = $this->insertSimpleProduct($trans_id, $list, 1);
				// Second Get associated_skus list from product master
				$master = $this->getApiProductInfo($sku, 1);
				$associated_skus = array($list['prod_info']['sku']);
				if(!empty($master["associated_skus"])){
					foreach ($master["associated_skus"] as $skus) {
						array_push($associated_skus, $skus);
					}
				}

				// Third update master configurable product
				if($child['transaction']['status']=='UPDATE-COMPLETED'){
					$master = $this->updateMasterConfigProduct($trans_id, $sku, $associated_skus);
				}

				// return value
				$result = $child;

			} else {
				// Have child product ready, then return
				$result = array(
					'auth'=>array('status'=>'COMPLETED', 'msg'=>''),
					'transaction'=>array('trans_id'=>$trans_id, 'status'=>'UPDATE-UN-COMPLETED', 'msg'=>'Have product already.')
						);
			}
		}

		return $result;
	}

	/**
	 * Update Configurable Product
	 * @param  string $trans_id
	 * @param  array $list
	 * @return array
	 */
	function updateConfigProduct($trans_id, $list){
		return $result = $this->updateSimpleProduct($trans_id, $list, 0);
	}

	/**
	 * Insert Master Configurable Product
	 * @param string $trans_id
	 * @param string $sku
	 * @param array $list
	 * @param array $associate
	 * @return array
	 */
	function addMasterConfigProduct($trans_id, $sku, $list, $associate){
		// Init Data
		$productId = 0;
		$params_option = $this->initOptionValue($list['prod_attr']);
		$params_attr = array();
		$params_inv = $this->_helper->initProductInventoryValue($list);
		$params = $this->_helper->initProductValue($list, $params_attr, $params_inv, 4, $associate);

		// Add Product data to Storview
		$msg = '';
		try {
			// Insert product to view default / en
				$productId = $this->_serviceV1->call($this->_sessionV1, 'product.create', array('configurable', $this->setDF, $sku, $params, $this->viewDF));
		} catch (SoapFault $e) {
			$msg = $e->faultstring;
		}

		// Update product attribute use api v2
		if($productId > 0) {
			// Init Data
			$params_attr = $this->_helper->initProductAttrMasterValue($list, $params_option, $this->viewDF);
			$params = array('additional_attributes'=>array('single_data'=>$params_attr));
			// Update product data to Storeview EN
			try {
					$productUpdate = $this->_service->catalogProductUpdate($this->_session, $productId, $params, $this->viewDF);
			} catch (SoapFault $e) {
				$msg = $e->faultstring;
			}

			// Init TH Variable
			$params = $this->_helper->initProductValueTH($list);
			// Update product data to Storeview TH
			try {
					$productUpdate = $this->_service->catalogProductUpdate($this->_session, $productId, $params, $this->viewTH);
			} catch (SoapFault $e) {
				$msg = $e->faultstring;
			}

			// Add Media Gallery
			$this->insertApiImagesGallery($productId, $list['prod_img']);

			if($productUpdate){
				// Insert product to all store view success.
				$result = array(
					'auth'=>array('status'=>'COMPLETED', 'msg'=>''),
					'transaction'=>array('trans_id'=>$trans_id, 'status'=>'UPDATE-COMPLETED', 'msg'=>'Insert To All StoreView Success.')
						);
			} else {
				// Can't insert product to store view th
				$result = array(
					'auth'=>array('status'=>'COMPLETED', 'msg'=>''),
					'transaction'=>array('trans_id'=>$trans_id, 'status'=>'UPDATE-COMPLETED', 'msg'=>'Insert ViewEN Success, ViewTH Not Success, Exception : '.$msg)
						);
			}

		} else {
			// Can't insert product to all store view
			$result = array(
				'auth'=>array('status'=>'COMPLETED', 'msg'=>''),
				'transaction'=>array('trans_id'=>$trans_id, 'status'=>'UPDATE-UN-COMPLETED', 'msg'=>'Can not insert product, Exception : '.$msg)
					);
		}

		return $result;
	}

	/**
	 * Update Master Configurable Product
	 * @param  string $trans_id
	 * @param  string $sku
	 * @param  array $associate
	 * @return array
	 */
	function updateMasterConfigProduct($trans_id, $sku, $associate){
		try {
				$productUpdate = $this->_serviceV1->call($this->_sessionV1, 'product.update', array($sku, array('associated_skus' => $associate)));
		} catch (SoapFault $e) {
			$msg = $e->faultstring;
			$productUpdate = false;
		}

		if($productUpdate){
			$result = array(
				'auth'=>array('status'=>'COMPLETED', 'msg'=>''),
				'transaction'=>array('trans_id'=>$trans_id, 'status'=>'UPDATE-COMPLETED', 'msg'=>'Update Master Configurable Product Success.')
				);
		} else {
			$result = array(
				'auth'=>array('status'=>'COMPLETED', 'msg'=>''),
				'transaction'=>array('trans_id'=>$trans_id, 'status'=>'UPDATE-UN-COMPLETED', 'msg'=>'Can not update master configurable product, Exception : '.$msg)
					);
		}

		return $result;
	}

	/**
	 * Update Master Configurable Product Info
	 * @param  string $trans_id
	 * @param  string $sku
	 * @param  array $associate
	 * @return array
	 */
	function updateMasterConfigProductInfo($trans_id, $productId, $list, $associate){
		// Init Data
		$params_option = $this->initOptionValue($list['prod_attr']);
		$params_attr = array();
		$params_inv = $this->_helper->initProductInventoryValue($list);
		$params = $this->_helper->initProductValue($list, $params_attr, $params_inv, 4, $associate);

		// Update product attribute use api v2
		if($productId) {
			// Init Data
			$params_attr = $this->_helper->initProductAttrMasterValue($list, $params_option, $this->viewDF);
			$params['additional_attributes'] = array('single_data'=>$params_attr);
			// Update product data to Storeview EN
			try {
				$productUpdate = $this->_service->catalogProductUpdate($this->_session, $productId, $params, $this->viewDF);
			} catch (SoapFault $e) {
				$msg = $e->faultstring;
				$productUpdate = false;
			}

			// Init TH Variable
			$params = $this->_helper->initProductValueTH($list);
			// Update product data to Storeview TH
			try {
				$productUpdate = $this->_service->catalogProductUpdate($this->_session, $productId, $params, $this->viewTH);
			} catch (SoapFault $e) {
				$msg = $e->faultstring;
				$productUpdate = false;
			}

			// Update Media Gallery
			$this->updateApiImagesGallery($productId, $list['prod_img']);				

		} else {
			// Can't insert product to all store view
			$productUpdate = false;
		}

		return $productUpdate;
	}

	// ================ Magento Soap Api Function ================ //
	/**
	 * Get Product Info
	 * @param  string $sku
	 * @param  integer $version
	 * @return array
	 */
	function getApiProductInfo($sku, $version){
		$result = array();
		if($version==1){
			try {
				$result = $this->_serviceV1->call($this->_sessionV1, 'catalog_product.info', $sku);
			} catch (SoapFault $e) {}
		} else {
			try {
				$result = $this->_service->catalogProductInfo($this->_session, $sku);
			} catch (SoapFault $e) {}
		}

		return $result;
	}

	/**
	 * Init OptionID value
	 * @param  array $option
	 * @return array
	 */
	function initOptionValue($option){
		$option_value = array();
		$option_value['product_color'] = $this->getApiAttributeOptionValue('product_color', $option['product_color_en'], $option['product_color_th']);
		$option_value['product_design'] = $this->getApiAttributeOptionValue('product_design', $option['product_design_en'], $option['product_design_th']);
		$option_value['product_group'] = $this->getApiAttributeOptionValue('product_group', $option['product_group_en'], $option['product_group_th']);
		$option_value['product_size'] = $this->getApiAttributeOptionValue('product_size', $option['product_size_en'], $option['product_size_th']);
		$option_value['product_type'] = $this->getApiAttributeOptionValue('product_type', $option['product_type_en'], $option['product_type_th']);

		return $option_value;
	}

	/**
	 * Init OptionID value
	 * @param  array $option
	 * @return array
	 */
	function initOptionStoreValue($option){
		$option_value = array();
		$option_value['product_find_in_shop'] = $this->getApiAttributeOptionStoreValue('product_find_in_shop', $option['product_find_in_shop']);

		return $option_value;
	}

	/**
	 * Get Attribute Option Value
	 * @param  string $label
	 * @param  string $opt_en
	 * @param  string $opt_th
	 * @return array
	 */
	function getApiAttributeOptionValue($label, $opt_en, $opt_th){
		if ($opt_en != '') {
			try {
				$options = $this->_service->catalogProductAttributeOptions($this->_session, $label);
			} catch (SoapFault $e) {}

			// Check option value
			foreach ($options as $opt) {
					if ($opt->label == $opt_en) {
							$option_val = $opt->value;
							break;
					}
			}

			// If No option data, then add new option
			if (!isset($option_val)) {
				$data = array(
					'label'=>array(
						array('store_id'=>array($this->viewDF, $this->viewEN), 'value'=>$opt_en),
						array('store_id'=>array($this->viewTH), 'value'=>$opt_th)
					),
					'order'=>count($options),
					'is_default'=>0
				);

				try {
						$new_options = $this->_service->catalogProductAttributeAddOption($this->_session, $label, $data);
					} catch (SoapFault $e) {}
			}

			// Get new option value
			try {
				$options = $this->_service->catalogProductAttributeOptions($this->_session, $label);
			} catch (SoapFault $e) {}

			// Check option value
			foreach ($options as $opt) {
					if ($opt->label == $opt_en) {
							$option_val = $opt->value;
							break;
					}
			}

			return $option_val;

		} else {
			return null;
		}
	}

	/**
	 * Get Attribute Option Value
	 * @param  string $label
	 * @param  string $opt_en
	 * @param  string $opt_th
	 * @return array
	 */
	function getApiAttributeOptionStoreValue($label, $opt){
		$option_val = [];
		if($opt != '') {
			try {
				$options = $this->_service->catalogProductAttributeOptions($this->_session, $label);
				$optArr = explode(',', $opt);
				// Check option value
				foreach ($options as $option) {
					if(in_array($option->label, $optArr)){
						$option_val[] = $option->value;
					}
				}
			} catch (SoapFault $e) {}			
		}
		
		return $option_val;
	}

	/**
	 * Insert Product Media Gallery
	 * @param  string $productId
	 * @param  array $list
	 */
	function insertApiImagesGallery($productId, $list){
		if(!empty($list)) {
			$i=1;
			foreach ($list as $image) {
				if($image['content']!=''){
					try {
							$result = $this->_service->catalogProductAttributeMediaCreate(
							$this->_session,
							$productId,
							array('file' => array('content'=>$image['content'], 'mime'=>$image['mine']),
								'label' => '',
								'position' => $i,
								'types' => ($i==1 ? array('image', 'small_image', 'thumbnail') : array()),
								'exclude' => 0
							)
						);
					} catch (SoapFault $e) {} $i++;
				}
			}
		}
	}

	/**
	 * Update Product Media Gallery
	 * @param  string $productId
	 * @param  array $list
	 */
	function updateApiImagesGallery($productId, $list){
		if(!empty($list)) {
			// Get Old Media List
			try{
				$image = $this->_service->catalogProductAttributeMediaList($this->_session, $productId);
			} catch (SoapFault $e) {}
			// Remove Old Image
			if(!empty($image)){
				foreach ($image as $k => $img) {
					try{
						$remove = $this->_service->catalogProductAttributeMediaRemove($this->_session, $productId, $img->file);
					} catch (SoapFault $e) {}
				}
			}
			// Then Add New Image
			$this->insertApiImagesGallery($productId, $list);
		}
	}

	/**
	 * [initInventoryCaseProductUpdate description]
	 * @return [type] [description]
	 */
	function initInventoryCaseProductUpdate($sku){
			// Check Product sku in store view
		$msg = '';
		try {
				$inventory = $this->_service->catalogInventoryStockItemList($this->_session, array($sku));
		} catch (SoapFault $e) {
			$msg = $e->faultstring;
				$inventory = array();
		}

		if(!empty($inventory)){
			// Update inventory data
			foreach ($inventory as $key => $inv) {
				// Init inventory parameter.
				$qty = (int) $inv->qty;
			}
		} else {
			$qty = 0;
		}

		return $qty;
	}

	//tew change status by qty & store qty  2020-08-28
	function checkUpdateProductStatus($sku, $params, $type){
		Mage::app();
		umask(0); 
		$product = Mage::getModel('catalog/product')->loadByAttribute('sku', $sku);

		if($product){			
			$product_stock = (int) Mage::getModel('cataloginventory/stock_item')->loadByProduct($product)->getQty();
			$product_find_in_shop = $product->getData('product_find_in_shop');

			if($type == 'store'){
				$product_find_in_shop = (!empty($params['additional_attributes']['multi_data'][0]['value']) ? 'has' : '');
			}

			if( ($product_stock <= 0  || $product_stock == '')  && ($product_find_in_shop == '') ){
				// disable
				$params['status'] = '2';
			} else {
				$params['status'] = '1';
			}
		}

		return $params;
	}


		
}