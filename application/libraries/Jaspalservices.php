<?php
/*
 * Narenrit Hadsadintorn
 *
 * The first PHP Library to support jaspal webservices
 */
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(FCPATH.'vendor/autoload.php');
// require_once(str_replace('/JaspalServices/', '/', FCPATH).'app/Mage.php');

class Jaspalservices {
	private $ci;
	private $_client;
	private $_wsdl;

	public function __construct()
	{
		$this->ci =& get_instance();		
		$this->_wsdl = $this->ci->config->item('wsdl', 'webservices');
	}

	/** 
	 * connect to soap server
	 */
	public function connectSoap(){
		$this->_client = new nusoap_client($this->_wsdl, 'wsdl', false, false, false, false, 0, 1800000);
		$this->_client->soap_defencoding = 'UTF-8';
		$this->_client->decode_utf8 = false;

		return $this->_client;
	}
	/**
	 * call jaspal-home service MainJobProcDataApi
	 * @return array
	 */
	public function MainJobProcDataApi(){
		$data = $this->_client->call('MainJobProcDataApi');
		$result = (array) json_decode($data['MainJobProcDataApiResult'], true);
		return $result;
	}
	/**
	 * call jaspal-home service GetProdInfoApi
	 * @return array
	 */
	public function GetProdInfoApi($service, $params){
		$data = $this->_client->call($service, $params);
		if(isset($data[$service.'Result'])){
			$result = (array) json_decode($data[$service.'Result'], true);
		} else {
			$result = [];
		}

		return $result;
	}
	/**
	 * call jaspal-home service postUpdateSendFlagApi
	 * @return array
	 */
	public function PostUpdateSendFlagApi($params){
		$data = $this->_client->call('PostUpdateSendFlagApi', $params);
		$result = (array) json_decode($data['PostUpdateSendFlagApiResult'], true);
		return $result;
	}
		
}