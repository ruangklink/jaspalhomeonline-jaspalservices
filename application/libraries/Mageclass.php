<?php
/*
 * Narenrit Hadsadintorn
 *
 * The first PHP Library to support jaspal webservices
 */
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(FCPATH.'vendor/autoload.php');
require_once(str_replace('/JaspalServices/', '/', FCPATH).'app/Mage.php');
require_once(str_replace('/JaspalServices/', '/', FCPATH).'app/code/community/MDN/AutoCancelOrder/controllers/Adminhtml/AutoCancelOrder/AdminController.php');

class Mageclass {
	private $ci;

	public function __construct()
	{
		$this->ci =& get_instance();		
		$this->_wsdl = $this->ci->config->item('wsdl', 'webservices');
	}

	public function _applyRule(){		
    Mage::setIsDeveloperMode(true);
    umask(0);
    Mage::app();
    Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

    try {
      Mage::getModel('catalogrule/rule')->applyAll();
     	Mage::app()->removeCache('catalog_rules_dirty');
    } catch (Exception $exception) {}
	}

	public function _applyAutoCancelOrder(){
		Mage::setIsDeveloperMode(true);
		umask(0);
		Mage::app();
		Mage::app()->setCurrentStore(Mage_Core_Model_App::ADMIN_STORE_ID);

		try {
			$controller = new MDN_AutoCancelOrder_Adminhtml_AutoCancelOrder_AdminController(
				Mage::app()->getRequest(),
				Mage::app()->getResponse()
			);
			$request = $controller->getRequest();
			$response = $controller->getResponse();
			$controller->applyOnOrderAction();
		} catch (Exception $exception) {}
	}

	public function __debugg($dd = []){
		Mage::app();
		$list = json_decode($dd['data']);
		$product = Mage::getModel('catalog/product')->loadByAttribute('sku', $list->PROD_CODE);

		if($product){
			$mapp = ['SP'=>'1720','EM'=>'1721','CB'=>'1722'];
			if($list->PROD_CODE){
				$opt = explode(',',$list->StockQty);
				$newopt = [];
				foreach ($opt as $v) {
					$newopt[] = $mapp[$v];
				}
				$options = implode(",",$newopt);
			} else {
				$options = '';			
			}
	
			$product->setData('product_find_in_shop', $options);
			$product->save();
			return 'update product: ' . $list->PROD_CODE . ' set store: ' . $options . '<hr>';
		} else {
			return 'no product: ' . $list->PROD_CODE . ' in db<hr>';
		}

		// $collection = Mage::getModel('catalog/product')->getCollection()->addFieldToFilter('status', 1);
		// $collection->joinField('category_id', 'catalog_category_product', 'category_id', 'product_id=entity_id', ['category_id'=>'154'], 'left');
		// $collection->addAttributeToFilter('category_id', [['null' => true]]);
		// foreach ($collection as $_product) {
		// 	$categoryIds = $_product->getCategoryIds();
		// 	$newCate = [];			
		// 	$flag = false;
		// 	foreach ($categoryIds as $value) {
		// 		$newCate[] = intVal($value);
		// 		if(intVal($value) == 154){
		// 			$flag = true;
		// 		}
		// 	}
		// 	if(!$flag){
		// 		echo 'do';
		// 		$newCate[] = 154;
		// 		$product = Mage::getModel('catalog/product')->load($_product->getId());
		// 		$product->setCategoryIds($newCate);
		// 		$product->save();
		// 		echo 'Add cate product: '.$_product->getSku(). '<br>';
		// 	}
		// }

	}
		
}