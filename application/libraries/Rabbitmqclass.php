<?php
/*
 * Narenrit Hadsadintorn
 *
 * The first PHP Library to support Rabbitmq
 */
defined('BASEPATH') OR exit('No direct script access allowed');

require_once(FCPATH.'vendor/autoload.php');

use PhpAmqpLib\Connection\AMQPStreamConnection;
use PhpAmqpLib\Message\AMQPMessage;


class Rabbitmqclass {
	private $connection;
	private $msg;
	private $ci;

	public function __construct()
	{
		$this->ci =& get_instance();		
	}

	/** 
	 * connect to rabbitmq server
	 */
	public function connection(){
		return $this->connection = new AMQPStreamConnection(
			$this->ci->config->item('host', 'rabbitmq'),
			$this->ci->config->item('port', 'rabbitmq'),
			$this->ci->config->item('user', 'rabbitmq'),
			$this->ci->config->item('pass', 'rabbitmq'),
			$this->ci->config->item('vhost', 'rabbitmq')
		);
	}

	/** 
	 * connect to rabbitmq server
	 */
	public function addAMQPMessage($data){
		return $this->msg = new AMQPMessage($data, array('delivery_mode' => 2));
	}

}