<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">
	<!-- Sidebar - Brand -->
	<a class="sidebar-brand d-flex align-items-center justify-content-center" href="<?php echo site_url('index.php/dashboard'); ?>">
		<div class="sidebar-brand-icon rotate-n-15">
			<i class="fas fa-laugh-wink"></i>
		</div>
		<div class="sidebar-brand-text mx-3">JaspalService</div>
	</a>
	<!-- Divider -->
	<hr class="sidebar-divider my-0">
	<!-- Nav Item - Dashboard -->
	<li class="nav-item active">
		<a class="nav-link" href="<?php echo site_url('index.php/dashboard'); ?>">
			<i class="fas fa-fw fa-tachometer-alt"></i>
			<span>Dashboard</span></a>
	</li>
	<!-- Divider -->
	<hr class="sidebar-divider">
	<!-- Heading -->
	<div class="sidebar-heading">
		Menu
	</div>       
	<!-- Nav Item - Connect -->
		<li class="nav-item">
		<a class="nav-link" href="<?php echo site_url('index.php/dashboard/connection'); ?>">
			<i class="fas fa-fw fa-chart-area"></i><span>Connection Logs</span>
		</a>
	</li>
	<!-- Nav Item - Queue -->
	<li class="nav-item">
		<a class="nav-link" href="<?php echo site_url('index.php/dashboard/queue'); ?>">
			<i class="fas fa-fw fa-chart-area"></i><span>Queue Process Logs</span>
		</a>
	</li>			
	<!-- Nav Item - Transaction -->
	<li class="nav-item">
		<a class="nav-link" href="<?php echo site_url('index.php/dashboard/transaction'); ?>">
			<i class="fas fa-fw fa-chart-area"></i><span>Transaction Logs</span>
		</a>
	</li>
	<!-- Nav Item - Api -->
	<li class="nav-item">
		<a class="nav-link" href="<?php echo site_url('index.php/dashboard/api'); ?>">
			<i class="fas fa-fw fa-chart-area"></i><span>API Process Logs</span>
		</a>
	</li>
	<!-- Nav Item - Daily Sales -->
	<li class="nav-item">
		<a class="nav-link" href="<?php echo site_url('index.php/dashboard/daily'); ?>">
			<i class="fas fa-fw fa-chart-area"></i><span>Daily Sales Logs</span>
		</a>
	</li>

	<!-- Divider -->
	<hr class="sidebar-divider d-none d-md-block">
	<!-- Sidebar Toggler (Sidebar) -->
	<div class="text-center d-none d-md-inline">
		<button class="rounded-circle border-0" id="sidebarToggle"></button>
	</div>
</ul>