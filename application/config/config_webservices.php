<?php
	defined('BASEPATH') OR exit('No direct script access allowed');
	/**
	 * Config for Jaspal Webservice
	 */
	$config['webservices'] = array(
		'wsdl' => 'http://202.183.232.197/EComwebservice/BussinessFunc/EComService.asmx?wsdl', // prod
		//'wsdl' => 'http://202.183.232.197/EComwebserviceTest/BussinessFunc/EComService.asmx?wsdl', // test
		'api_user' => 'admin_api',
		'api_key' => 'S3K4B1W7jQA7KK5GiL33otDQZadOB4OO',
		'view_df' => 0,
		'view_en' => 3,
		'view_th' => 1,
		'set_df' => 4,
		'web_df' => 1,
		'find_in_shop_id' => 154,
		//'soap_service_v1_url' => 'https://jaspaluat.campaignserv.com/api/soap/?wsdl=1',
		//'soap_service_v2_url' => 'https://jaspaluat.campaignserv.com/api/v2_soap/?wsdl=1',
		'soap_service_v1_url' => 'https://www.jaspalhomeonline.com/api/soap/?wsdl=1',
		'soap_service_v2_url' => 'https://www.jaspalhomeonline.com/api/v2_soap/?wsdl=1'
	);