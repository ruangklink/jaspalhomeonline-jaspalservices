<?php
	/**
	 * Database Helper Class
	 * Use for support api service
	 * Copyright (c) 2016 @Adyim
	 * @category   DBHelper
	 * @package    ProductManagementWebservice
	 * @copyright  Copyright (c) 2016 @Adyim
	 * @license    None
	 * @version    1.0.0, 2016-07-01
	 */

	require_once(Mage::getBaseDir().'/JaspalServices/outhelpers/config.php');

	class DBHelper {
		/* Init connect variable */
	  protected $config_host = _DBHOST;
		protected $config_dbuser = _DBUSER;
		protected $config_dbpass = _DBPASS;
		protected $config_dbname = _DBNAME;
		protected $connection;

	  public function __construct() {
	   	$this->connect();
	  }

	  /**
	    * Database connection
	    */
	  public function connect() {
	  	$this->connection = new mysqli($this->config_host, $this->config_dbuser, $this->config_dbpass, $this->config_dbname);
	   	$this->connection->set_charset("utf8");
		}

		/**
		 * Close database connection
		 */
		public function closeConnect(){
			$this->connection->close();
		}

		/**
		 * insert daily sale log function
		 * @param  $order_id
		 * @param  $log
		 * @return $id
		 */
		public function insertDailyLog($order_id, $invoice_id, $data, $log='') {
			if ($this->connection->connect_errno) {
				$id = 0;
			} else {
				// insert to db
				if ($stmt = $this->connection->prepare('INSERT INTO adyim_webservices_dailysales(order_id, invoice_id, data, log, created_date) VALUES (?,?,?,?,NOW())')) {
					$stmt->bind_param('ssss', $order_id, $invoice_id, $data, $log);
					$stmt->execute();
					$stmt->close();
				}

				$id = $this->connection->insert_id;
			}

			return $id;
		}

		/**
		 * [updateDailyLog description]
		 * @param  [type] $order_id   [description]
		 * @param  [type] $invoice_id [description]
		 * @param  [type] $data       [description]
		 * @param  string $log        [description]
		 * @return [type]             [description]
		 */
		public function updateDailyLog($id, $log=''){
			if ($this->connection->connect_errno) {
				//
			} else {
				// update to db
				if ($stmt = $this->connection->prepare('UPDATE adyim_webservices_dailysales SET log=?, created_date=NOW() WHERE id=?')) {
					$stmt->bind_param('ss', $log, $id);
					$stmt->execute();
					$stmt->close();
				}
			}
		}

		/**
		 * [insertSCBLog description]
		 * @param  array  $resp [description]
		 * @return [type]       [description]
		 */
		public function insertSCBLog($resp = array()) {
			if ($this->connection->connect_errno) {
				$id = 0;
			} else {
				// insert to db
				$data = json_encode($resp);
				if ($stmt = $this->connection->prepare('INSERT INTO adyim_webservices_scb(testdata, created_date) VALUES (?,NOW())')) {
					$stmt->bind_param('s', $data);
					$stmt->execute();
					$stmt->close();
				}

				$id = $this->connection->insert_id;
			}

			return $id;
		}

	}