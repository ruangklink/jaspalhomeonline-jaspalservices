<?php
	/**
	 * Daily Service Helper Class
	 * Use for support api service
	 * Copyright (c) 2016 @Adyim
	 * @category   DailySaleHelper
	 * @package    ProductManagementWebservice
	 * @copyright  Copyright (c) 2016 @Adyim
	 * @license    None
	 * @version    1.0.0, 2016-07-01
	 */

	require_once(Mage::getBaseDir().'/JaspalServices/outhelpers/config.php');
	require_once(Mage::getBaseDir().'/JaspalServices/outhelpers/lib/nusoap/src/nusoap.php');
	require_once(Mage::getBaseDir().'/JaspalServices/outhelpers/helpers/DBHelper.php');

	class DailySaleHelper {
		protected $_wsdl = _WSDL;
		protected $_client;
		protected $_db;

		public function __construct() {
			$this->_client = new nusoap_client($this->_wsdl, 'wsdl', false, false, false, false, 0, 1800000);
			$this->_client->soap_defencoding = 'UTF-8';
			$this->_client->decode_utf8 = false;
			$this->_db = new DBHelper();
		}

		/**
		 * daily sale api function
		 *
		 * @param      array   $orderId  The order identifier
		 * @param      array   $headers  The headers
		 * @param      array   $details  The details
		 * @return     boolean  ( description_of_the_return_value )
		 */
		public function execute($orderId, $invoiceId, $headers, $details) {
			if($orderId){
				// init data
				$head = '';
				foreach ($headers as $header) {
					$head .= $header;
					$head .= '|';
				}

				$data = array();
				foreach ($details as $detail) {
					$data[] = $head.$detail['ITEM'].'|'
						.$detail['BAR_CODE'].'|'
						.$detail['PROD_CODE'].'|'
						.$detail['PROD_DESC'].'|'
						.$detail['UOM_CODE'].'|'
						.$detail['UNIT_PRICE'].'|'
						.$detail['DISC_RATE'].'|'
						.$detail['DISC_AMT'].'|'
						.$detail['VAT_AMT'].'|'
						.$detail['NET_AMT'].'|'
						.$detail['AMT'].'|'
						.$detail['QTY'].'|'
						.$detail['SALE_PD'].'|'
						.$detail['PROD_STATUS'];
				}

				// loop call daily sale service
				$row = count($data);
				foreach ($data as $val) {
					// save log before call service.
					$logid = $this->_db->insertDailyLog($orderId, $invoiceId, $val, 'Before Call Services.');
					// call service.
						$result = $this->_createDailySaleApi($val, $row);
					// update log after send service
					if($result==false){
						$this->_db->updateDailyLog($logid, 'Services has die in ERP');
					} else {
						$this->_db->updateDailyLog($logid, $result['CreateDailySaleApiResult']);
					}
				}

				return true;

			} else {
				return false;
			}
		}

		/**
		 * [executeTest description]
		 * @param  array  $resp [description]
		 * @return [type]       [description]
		 */
		public function executeScbLog($resp = array()) {
			$result = $this->_db->insertSCBLog($resp);
		}

		/**
		 * call service function
		 * @param  string $daily
		 * @param  integer $record
		 */
		function _createDailySaleApi($daily, $record){
			$client = new nusoap_client($this->_wsdl, 'wsdl', false, false, false, false, 0, 1800000);
			$client->soap_defencoding = 'UTF-8';
			$client->decode_utf8 = false;

			$params = array(
				'DailySaleData'=>$daily,
				'TotalRecord'=>$record
			);

			$result = false;

			try {
					$result = $client->call('CreateDailySaleApi', $params);
			} catch(SoapFault $f) {
				// handle issues returned by the web service
			} catch(Exception $e) {
				// handle PHP issues with the request
			}

			return $result;
		}

	}